const Joi = require('joi');
const express = require('express');
const app = express();

app.use(express.json());

var courses = [
    { 'id' : 1,  'name' : 'shiam' },
    { 'id' : 2,  'name' : 'sadman' },
    { 'id' : 3,  'name' : 'ayman' }
];

app.get('/', (req, res) => {
    res.send('Hello world again for express');
});

app.get('/api/courses', (req, res) => {
    res.send([1,2,3]);
});

app.get('/api/hello/:name/:age', (req, res) => {
    //res.send(req.params);
    res.send(req.query);
});

app.get('/api/courses/:id', (req, res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if(!course)
        res.status(404).send('not found!!');
    else
        res.send(course);

});

app.post('/api/courses', (req, res) => {
    const { error } = validateCourse(req.body);

    if(error) return res.status(400).send(error.details[0].message);
        
    const course = {
        id: courses.length + 1,
        name: req.body.name 
    };
    courses.push(course);
    res.send(course);
});

app.put('/api/courses/:id', (req, res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if(!course) return res.status(404).send('not found!!');

    const { error } = validateCourse(req.body);

    if(error) return res.status(400).send(error.details[0].message);
        
    

    course.name = req.body.name;
    res.send(course);

});

app.delete('/api/courses/:id', (req, res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if(!course) return res.status(404).send('not found!!');

    const index = courses.indexOf(courses);
    courses.splice(index, 1);

    res.send(course);
});



function validateCourse(course){
    const schema = {
        name: Joi.string().min(3).required()
    };

    return Joi.validate(course, schema);
}



const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listing to port ${port}....`));